package tech.blimop.grupo7.banco.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import  tech.blimop.grupo7.banco.entity.*;

@Repository
public interface MovimientoRepository extends CrudRepository <Movimiento, Long>{

}
