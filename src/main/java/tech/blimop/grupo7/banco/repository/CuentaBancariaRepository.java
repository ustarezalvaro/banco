package tech.blimop.grupo7.banco.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import tech.blimop.grupo7.banco.entity.CuentaBancaria;
import tech.blimop.grupo7.banco.entity.Movimiento;

@Repository
public interface CuentaBancariaRepository extends CrudRepository<CuentaBancaria, Long>{
	
	List<Movimiento> findById(long id);

}
