package tech.blimop.grupo7.banco.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import tech.blimop.grupo7.banco.entity.Cliente;
import tech.blimop.grupo7.banco.entity.CuentaBancaria;

import java.util.List;

@Repository
public interface ClienteRepository extends CrudRepository <Cliente, Long>{
	
	Cliente findByNombre(String nombre);
	
	

	List<CuentaBancaria> findByDni(String dni);
	
	
}
