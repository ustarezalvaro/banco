package tech.blimop.grupo7.banco.service;

import java.util.List;

import tech.blimop.grupo7.banco.entity.Cliente;
import tech.blimop.grupo7.banco.entity.CuentaBancaria;


public interface ClienteService {

	List<CuentaBancaria> getCuentasBancarias(String dni);
	
	

	CuentaBancaria addCuentaBancaria(CuentaBancaria cuentaBancaria);
	
	CuentaBancaria updateCuentaBancaria(CuentaBancaria cuentaBancaria);

	Cliente save(Cliente cliente);
	Cliente findById(Long id);
	void deleteCliente(Long id);

}