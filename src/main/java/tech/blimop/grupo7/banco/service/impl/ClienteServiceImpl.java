package tech.blimop.grupo7.banco.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.blimop.grupo7.banco.entity.Cliente;
import tech.blimop.grupo7.banco.entity.CuentaBancaria;
import tech.blimop.grupo7.banco.repository.ClienteRepository;
import tech.blimop.grupo7.banco.repository.CuentaBancariaRepository;
import tech.blimop.grupo7.banco.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService{
	@Autowired
	private CuentaBancariaRepository cuentaBancariaRepository;
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Override
	public List<CuentaBancaria> getCuentasBancarias(String dni) {

		return clienteRepository.findByDni(dni);
	}



	@Override
	public CuentaBancaria updateCuentaBancaria(CuentaBancaria cuentaBancaria) {
		
		cuentaBancariaRepository.save(cuentaBancaria);
		return cuentaBancaria;
	}
	
	@Override
	public Cliente save(Cliente cliente) {
		return clienteRepository.save(cliente);
	}
		

	

	@Override
	public void deleteCliente(Long id) {
		// TODO Auto-generated method stub
		clienteRepository.deleteById(id);
		
	}

	@Override
	public Cliente findById(Long id) {
		
		Optional o =  clienteRepository.findById(id);
		if (o.isPresent())
			return (Cliente) o.get();
		return null;
	}



	@Override
	public CuentaBancaria addCuentaBancaria(CuentaBancaria cuentaBancaria) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
