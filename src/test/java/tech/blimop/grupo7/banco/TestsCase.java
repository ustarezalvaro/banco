package tech.blimop.grupo7.banco;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import tech.blimop.grupo7.banco.entity.Cliente;
import tech.blimop.grupo7.banco.entity.CuentaBancaria;
import tech.blimop.grupo7.banco.service.CuentaBancariaService;

@SpringBootTest
public class TestsCase {
	@Autowired
	CuentaBancariaService CuentaBancariaS;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test() {
		CuentaBancaria dto = new CuentaBancaria();
		Cliente cliente = new Cliente();
		cliente.setId(2L);
		dto.setNumeroCuenta("48758974589845");
		dto.setSaldo(6526);
		//dto.setFechaCreacion("05-11-2013"); 
		dto.setCliente(cliente);
		
		CuentaBancariaS.save(cliente);
		CuentaBancariaS.addCuentaBancaria(dto);
	}
}
